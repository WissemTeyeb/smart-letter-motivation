const ParcelProxyServer = require('parcel-proxy-server')

const managementGateway = {
  target: 'https://develop.prosperus.tech:4003',
  changeOrigin: true,
  headers: { host: 'localhost:1234' },
  cookieDomainRewrite: { '*': 'localhost' },
}

const managementMpEx = {
  target: 'http://164.68.109.207:5923',
  changeOrigin: true,
  headers: { host: 'localhost:1235' },
  cookieDomainRewrite: { '*': 'localhost' },
}



const server = new ParcelProxyServer({
  parcelOptions: { publicURL: '/smart-motivation' },
  entryPoint: './src/index.html',
  proxies: {
    '/auth': managementGateway,
    '/management': managementGateway,
    '/uaa': managementGateway,
    '/cloudwallet': managementGateway,
    '/dcn': managementGateway,
    '/dis': managementGateway,
    '/1.0': managementMpEx,
  },
})

server.bundler.on('buildEnd', () => {
  console.log('Build completed!')
  if (process.env.NODE_ENV === 'development') {
    console.log(`development detected using user '${process.env.LOGIN}'`)
  }
})
// start up the server
server.listen(1236, () => {
  console.log(
    'Parcel proxy server has started, serving on http://localhost:1236'
  )
})

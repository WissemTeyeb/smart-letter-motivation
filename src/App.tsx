import * as React from "react";
import { IntlProvider } from "react-intl";
import data from "./translations/data";
import Home from "./components/Home/Home";

const App = ( ) => (

  <IntlProvider locale={"fr"} messages={data["fr"]}>
    <Home />
  </IntlProvider>
);
export default App

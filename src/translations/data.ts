const data = {
  en: {
    assetFrom: " for sale",
    assetTo: "Accepted assets",
    offerProvider: "Offer owner",
    offerAmount: "Offer amount",
    swap: "swap",
    more: "More",
    rate: "Rate ",
    addRate: "Add ",
    createofferSuccessResponse: "The creationd of new offer  was succeed ",
    createofferFailedResponse: "The creationd of new offer was faild",
    swapofferFailedResponse: "The swap operation   was faild ",
    swapofferSuccessResponse: "The swap operation   was succeed ",
    personalSetting: "Personal settings"
  },
  fr: {
    personalSetting: "paramètres personnels",
    offerAmount: "Montant de l'offre",
    rate: "Taux : ",
    addRate: "Ajouter ",
    swap: "échanger",
    createofferSuccessResponse: "La création d'un nouveau offre  a été réussi",
    createofferFailedResponse: "La création d'un nouveau offre   a été échoué",
    swapofferFailedResponse: "L'opération déchange a été échoué ",
    swapofferSuccessResponse: "L'opération déchange a été réussi",
    addWalletSuccessResponse: "L'ajout d'un nouveau portefeuille a été réussi",
    addWalletFailedResponse: "L'ajout d'un nouveau portefeuille a été échoué",
    assetFrom: "à vendre: ",
    assetTo: "Actif accepté",
    offerProvider: "Propriétaire de l'offre: "
  }
};

export default data;

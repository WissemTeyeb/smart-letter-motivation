export { Err, Result, Ok, result } from "./Result";
export { Loading, NotRequested, Loadable, loadable } from "./Loadable";
export { MemoedC, Memoed, memoed } from "./Memoed";
export * from "./Actions";
export * from "./Codec";
export * from "./Assets";
export * from "./Transaction";
export * from "./Profile";
export * from "./UserState";

export type ValueOf<T> = T[keyof T];

export type Unpacked<T> = U<U<U<U<U<U<T>>>>>>;

export type U<T> = T extends (...args: any[]) => infer U
  ? U
  : T extends Promise<infer U>
  ? U
  : T;

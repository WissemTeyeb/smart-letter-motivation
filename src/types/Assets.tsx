import { Typography } from "antd";
import { array, option, record } from "fp-ts";
import { pipe } from "fp-ts/lib/pipeable";
import * as t from "io-ts";
import * as React from "react";
import { IntlContext, IntlShape } from "react-intl";
import { connect } from "react-redux";

import { loadable } from ".";
import { Locale } from "../locale/Locale";
import { State } from "../store";

export const Signature = t.type(
  { signature: t.string, clauses: t.array(t.string) },
  "Signature"
);
export type Signature = t.TypeOf<typeof Signature>;

export const Asset = t.type({
  dcnNymID: t.string,
  format: t.array(t.string),
  issuerNymID: t.string,
  name: t.string,
  protocolVersion: t.literal("dcn1.0"),
  signatures: t.record(t.string, Signature),
  terms: t.string,
  type: t.literal("unit")
});
export type Asset = t.TypeOf<typeof Asset>;

const divAndSymbol = (asset?: Asset): { divider: number; symbol: string } =>
  pipe(
    asset,
    option.fromNullable,
    option.chain(a =>
      a.name === "TND"
        ? option.some({ divider: 1000, symbol: "TND" })
        : pipe(
            a.format,
            array.head,
            option.map(fmt => ({
              divider: parseInt(fmt.substring(1, fmt.indexOf(" ")), 10),
              symbol: fmt.substring(fmt.indexOf(" ") + 1)
            }))
          )
    ),
    option.getOrElse(() => ({ divider: 1, symbol: "" }))
  );

export const formatAmount = (
  intl: IntlShape,
  amount: number,
  asset?: Asset
) => {
  const { divider, symbol } = divAndSymbol(asset);
  const decimalPoints = Math.log10(divider);

  return {
    symbol,
    amount: intl.formatNumber(amount / divider, {
      minimumFractionDigits: decimalPoints,
      maximumFractionDigits: decimalPoints
    })
  };
};

export const extractSymbol = (amount: string): [string, string | undefined] => {
  const [a, s] = amount.split(" ");
  return [a, s];
};

export const parseAmount = (
  locale: Locale,
  amount: string,
  assets: { [unitID: string]: Asset },
  unitID?: string
): { amount: number; unitID?: string } => {
  const [a, s] = extractSymbol(amount.trim());
  const [assetID, asset] = pipe(
    assets,
    record.toArray,
    array.findFirst(([_, a]) => divAndSymbol(a).symbol === s),
    option.getOrElse(() => [unitID, !!unitID ? assets[unitID] : undefined])
  );

  const { divider } = divAndSymbol(asset);
  const n =
    Number(locale === "fr" ? a.replace(".", "").replace(",", ".") : a) *
    divider;

  if (!Number.isInteger(n)) {
    return { amount: NaN, unitID: !!assetID ? assetID : undefined };
  }
  return { amount: n, unitID: !!assetID ? assetID : undefined };
};

const mapStateToProps = (s: State) => ({
  assets: s.wallets.assets
});

type OwnProps = {
  asset?: Asset;
  amount?: number;
  unitID?: string;
  component?: React.ElementType;
} & React.ComponentProps<typeof Typography>;

type Props = ReturnType<typeof mapStateToProps> & OwnProps;

const AmountView = ({ amount = 0, asset, assets, unitID }: Props) => {
  const unit = pipe(
    option.fromNullable(asset),
    option.alt(() =>
      pipe(assets, loadable.lookup(unitID || ""), loadable.toOption)
    )
  );
  return (
    <IntlContext.Consumer>
      {intl => {
        const { amount: formattedAmount, symbol } = formatAmount(
          intl,
          amount,
          option.toUndefined(unit)
        );
        return (
          <Typography>
            <span style={{ fontSize: "100%" }}>{formattedAmount} </span>
            <span style={{ fontSize: "70%" }}>{symbol}</span>
          </Typography>
        );
      }}
    </IntlContext.Consumer>
  );
};

export const Amount = connect(mapStateToProps)(AmountView);

import * as t from "io-ts";
import { Transfer } from "./Transaction";

interface RIBBrand {
  readonly RIB: symbol;
}

export const RIB = t.brand(
  t.string,
  (s): s is t.Branded<string, RIBBrand> => s.length === 20,
  "RIB"
);
export type RIB = t.TypeOf<typeof RIB>;

export const CashOut = t.type({
  nym: t.string,
  rib: RIB,
  transfer: Transfer
});
export type CashOut = t.TypeOf<typeof CashOut>;

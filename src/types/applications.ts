/* const hasAuthority = (role: string) => (s: State) =>
pipe(
  s,
  loadable.toOption,
  option.flatten,
  option.filter(a => array.elem(eqString)(role, a.authorities)),
  option.isSome
) */

export type Application = {
  name: "Backoffice" | "Dashboards" | "Wallets" | "Bills" | "Marketplace";
  href: string;
  icon: string;
};
export const applications: Application[] = [
  {
    name: "Wallets",
    icon: "wallet",

    href: "/web-wallet"
  },
  {
    name: "Marketplace",
    icon: "wallet",

    href: "/marketplace"
  },
  {
    name: "Bills",
    icon: "bills",

    href: "/prosper-bills"
  },
  {
    name: "Dashboards",
    icon: "line-chart",
    href: "/dashboard"
  },
  {
    name: "Backoffice",
    icon: "book",
    href: "/backoffice"
  }
];

import * as t from "io-ts";

export const Fraction = t.type({ numerator: t.number, denominator: t.number });
export type Fraction = t.TypeOf<typeof Fraction>;

export const FeeSchema = t.type({
  ratio: Fraction,
  cap: t.number,
  floor: t.number
});
export type FeeSchema = t.TypeOf<typeof FeeSchema>;

export const FeePolicy = t.type({
  schemas: t.record(t.string, FeeSchema),
  defaultSchema: FeeSchema,
  feeNymID: t.string
});
export type FeePolicy = t.TypeOf<typeof FeePolicy>;

export const emptyFeePolicy: FeePolicy = {
  schemas: {},
  defaultSchema: {
    ratio: { numerator: 0, denominator: 100 },
    floor: 0,
    cap: 0
  },
  feeNymID: ""
};

export const calculateFeesDue = (
  { ratio: { denominator, numerator }, cap, floor }: FeeSchema,
  a: number
) => Math.max(floor, Math.min(cap, Math.floor((numerator * a) / denominator)));

import { createStore, applyMiddleware, combineReducers } from "redux";
import {
  RouterState,
  connectRouter,
  routerMiddleware
} from "connected-react-router";
import reduxSaga from "redux-saga";
import * as effects from "redux-saga/effects";
import { createBrowserHistory } from "history";
import { composeWithDevTools } from "redux-devtools-extension";
import * as letterCreationType from './components/LettreCreation/types'
import * as letterCreationState from './components/LettreCreation/state'

/* import * as locale from "./locale/Locale"; */

export type Actions = letterCreationType.Actions;
function* initSaga() {
  yield effects.all([
  
  ]);
}
function* rootSaga() {
  yield effects.fork(initSaga);
  yield effects.all([

  ]);
}
const sagaMiddleware = reduxSaga();
export const history = createBrowserHistory({ basename: "/smart-motivation" });
export type State = {
  router: RouterState;
  letterCreationState:letterCreationType.State,

};

const store = createStore(
  combineReducers<State>({
    letterCreationState:letterCreationState.reducer,
    router: connectRouter(history),

  }),

  composeWithDevTools({ trace: true })(
    applyMiddleware(sagaMiddleware, routerMiddleware(history))
  )
);
sagaMiddleware.run(rootSaga);
export default store;

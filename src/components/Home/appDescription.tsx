import * as React from "react";
import { Carousel, Card, Row, Col } from "antd";
import descriptionImage from "../../assets/descriptionImage.png";
import wissem from "../../assets/wissem.jpg";
import fares from "../../assets/fares.jpg";

import styled from "styled-components";

const CarouselStyle = styled(Carousel)`
  background: #f4f1f0 !important;
`;

export const AppDescription = () => (
  <Row gutter={[8, 48]} style={{ marginTop: "2%" }}>
    <Col span={24}>
      <CarouselStyle autoplay>
        <div>
          <Card
            hoverable
            style={{ width: "100%", height: "auto" }}
            cover={<img src={wissem}></img>}
          >
            <Card.Meta
              title="TEYEB Wissem"
              description="wissem.teyeb@supcom.tn"
            />
          </Card>
        </div>
        <div>
          <Card
            hoverable
            style={{ width: "100%", height: 300 }}
            cover={<img src={fares}></img>}
          >
            <Card.Meta
              title="Guerfal fares"
              description="Fares.guerfal@supcom.tn"
            />
          </Card>
        </div>
        <div>
          <Card
            hoverable
            style={{ width: "100%", height: "auto" }}
            cover={<img src={descriptionImage}></img>}
          >
            <Card.Meta
              title="Europe Street beat"
              description="www.instagram.com"
            />
          </Card>
        </div>
      </CarouselStyle>
    </Col>
    <Col span={24}>
      <Card title="About" bordered={false} style={{ width: "100%" }}>
        <p>description of the site</p>
      </Card>
    </Col>
  </Row>
);

import * as React from "react";
import { Layout, Typography, Row, Col } from "antd";
import styled from "styled-components";
import { MenuItems } from "./headerItems";
import { AppDescription } from "./appDescription";
import LetterCreation from "../LettreCreation/LetterCreation";
const StyledHeader = styled(Layout.Header)`
  display: flex;
  width: 100%;

  align-items: center;

  & > h3 {
    margin-bottom: 0;
    color: white;
  }
`;
const LayoutContent = styled(Layout.Content)``;

const Home = () => (
  <Layout>
    <StyledHeader style={{ zIndex: 99 }}>
      <Typography.Title level={3}>Smart-Motivation</Typography.Title>

      <MenuItems />
    </StyledHeader>
    {/*  <LayoutContent style={{ padding: '0 50px' }}><AppDescription/></LayoutContent>
     */}
    <LayoutContent style={{ padding: "0 50px" }}>
      <LetterCreation />
    </LayoutContent>
    <Layout.Footer style={{ textAlign: "center" }}>
      copyright ©2020 Created by TEYEB Wissem
    </Layout.Footer>
  </Layout>
);
export default Home;

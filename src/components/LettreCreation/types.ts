import {
  actionCreator,
  PayloadAction,
  Action,
  payloadActionCreator,
  Loadable,
  Memoed,
  FailableAction,
  failableActionCreator,
} from "../../types";
import * as t from "io-ts";
import * as transferTypes from "../../types";

const Experience = t.type({
  postName: t.string,
  experienceNumber: t.number,
});
export type experience = t.TypeOf<typeof Experience>;

const TechnicalSkills = t.type({
  slill: t.string,
  certification: t.number,
});
export type technicalSkills = t.TypeOf<typeof TechnicalSkills>;

const SoftSkills = t.type({
  skillName: t.string,
  experience: t.number,
});
export type softSkills = t.TypeOf<typeof SoftSkills>;
const PersonalInfo = t.type({
  firstName: t.string,
  lastName: t.string,
  age: t.number,
  address: t.string,
  email: t.string,
  status: t.string,
  jobName: t.string,
  jobLokkingFor: t.string,
});
export type personalInfo = t.TypeOf<typeof PersonalInfo>;
const Profile = t.type({
  experiences: t.array(Experience),
  softSkills: t.array(SoftSkills),
  technicalSkills: t.array(TechnicalSkills),
  personalInfo: PersonalInfo,
});
export type Profile = t.TypeOf<typeof Profile>;
export const emptyOffer: Readonly<Profile> = {
  experiences: [],
  softSkills: [],
  technicalSkills: [],
  personalInfo: {
    firstName: "",
    lastName: "",
    age: 0,
    address: "",
    email: "",
    status: "",
    jobName: "",
    jobLokkingFor: "",
  },
};

const AcademicInfo = t.type({
  schollName: t.string,
  address: t.string,
  description: t.string,
});
export type academicInfo = t.TypeOf<typeof AcademicInfo>;
export type State = {
  experiences: experience[];
  softSkills: softSkills[];
  technicalSkills: technicalSkills[];
  academicInfo: academicInfo[];
  personalInfo: personalInfo;
  createLetterResult: Loadable<unknown>;
  letterInputs: Loadable<Profile>;
};

export type UpdateExperienceAction = PayloadAction<
  "CREATE_LETTER/update_experience",
  experience
>;
export const updateExperienceAction = payloadActionCreator<
  UpdateExperienceAction
>("CREATE_LETTER/update_experience");

export type UpdateTechnicalSkillsAction = PayloadAction<
  "CREATE_LETTER/update_technicalSkills",
  technicalSkills
>;
export const updateTechnicalSkillsAction = payloadActionCreator<
  UpdateTechnicalSkillsAction
>("CREATE_LETTER/update_technicalSkills");

export type UpdateSoftSkillsAction = PayloadAction<
  "CREATE_LETTER/update_softlSkills",
  softSkills
>;
export const updateSoftSkillsAction = payloadActionCreator<
  UpdateSoftSkillsAction
>("CREATE_LETTER/update_softlSkills");

export type UpdatePersonalInfoAction = PayloadAction<
  "CREATE_LETTER/update_personalInfo",
  personalInfo
>;
export const updatePersonalInfoAction = payloadActionCreator<
  UpdatePersonalInfoAction
>("CREATE_LETTER/update_personalInfo");

export type UpdateAcademicInfoAction = PayloadAction<
  "CREATE_LETTER/update_AcademicInfo",
  academicInfo
>;
export const updateAcademicInfoAction = payloadActionCreator<
UpdateAcademicInfoAction
>("CREATE_LETTER/update_AcademicInfo");

export type CreateletterAction = Action<"CREATE_LETTER/create_letter">;
export const createletterAction = actionCreator<CreateletterAction>(
  "CREATE_LETTER/create_letter"
);
export type Actions =
  | UpdateExperienceAction
  | UpdatePersonalInfoAction
  | UpdateSoftSkillsAction
  | UpdateTechnicalSkillsAction
  | UpdateAcademicInfoAction
  | CreateletterAction;

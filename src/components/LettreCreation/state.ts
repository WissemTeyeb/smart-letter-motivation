import * as t from "./types";
import * as api from "./api";
import * as effects from "redux-saga/effects";
import { State } from "../../store";
import { pipe } from "fp-ts/lib/pipeable";
import { LocationChangeAction } from "connected-react-router";

import { Loading, foldEitherToAction, NotRequested, memoed } from "../../types";
import { lens } from "lens.ts";
import { CancelToken } from "axios";
import { takeLatest } from "../../saga";
import { matchPath } from "react-router";
import { task, either, array } from "fp-ts";

const _l = lens<t.State>();

export const initialState: t.State = {
  createLetterResult: NotRequested,
  letterInputs: NotRequested,
  experiences: [],
  softSkills: [],
  academicInfo: [],
  technicalSkills: [],
  personalInfo: {
    firstName: "",
    lastName: "",
    age: 0,
    address: "",
    email: "",
    status: "",
    jobName: "",
    jobLokkingFor: "",
  } ,
};

export const reducer = (s = initialState, a: t.Actions): t.State => {
  switch (a.type) {
    case "CREATE_LETTER/update_experience":
      return {
        ...s,
        experiences: [...s.experiences, a.payload],
      };
    case "CREATE_LETTER/update_personalInfo":
      return {
        ...s,
        personalInfo: a.payload,
      };
      case "CREATE_LETTER/update_AcademicInfo":
          return {
            ...s,
            academicInfo: [...s.academicInfo, a.payload],
        };
    case "CREATE_LETTER/update_softlSkills":
      return {
        ...s,
        softSkills: [...s.softSkills, a.payload],
      };
    case "CREATE_LETTER/update_technicalSkills":
      return {
        ...s,
        technicalSkills: [...s.technicalSkills, a.payload],
      };
    default:
      return s;
  }
};

export function* saga() {
  yield effects.all([]);
}

import * as React from "react";
import { Steps, Col, Row, Form, Input, Button } from "antd";
import { State } from "../../store";
import { connect } from "react-redux";

import {
  updateExperienceAction,
  updateSoftSkillsAction,
  updateTechnicalSkillsAction,
  updatePersonalInfoAction,
  updateAcademicInfoAction,
} from "./types";
const { Step } = Steps;
const layout = {
  labelCol: { span: 12 },
  wrapperCol: { span: 20 },
};

const mapStateToProps = (s: State) => ({
  createLaterResult: s.letterCreationState.createLetterResult,
  experiences:s.letterCreationState.experiences,
  personalInfo:s.letterCreationState.personalInfo,
  technicalSkills:s.letterCreationState.technicalSkills,
  academicInfo:s.letterCreationState.academicInfo,
  softSkills:s.letterCreationState.softSkills,
});
const mapDispatchToProps = {
  updateExperienceAction: updateExperienceAction,
  updateSoftSkillsAction: updateSoftSkillsAction,
  updateTechnicalSkillsAction: updateTechnicalSkillsAction,
  updatePersonalInfoAction: updatePersonalInfoAction,
  updateAcademicInfoAction: updateAcademicInfoAction,
};

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

type ownFormProps = {
  setCurrentSteps: React.Dispatch<React.SetStateAction<number>>;
  currentSteps: number;
  update: any;
  data:any;
};
const ExperienceForm = ({ setCurrentSteps, currentSteps,update,data}: ownFormProps) => (
  <Form {...layout}
  onFinish={values=> update(values) 

  }
  
  >
    <Form.Item name="postName" label="Post name" rules={[{ required: true }]}>
      <Input />
    </Form.Item>
    <Form.Item
      name="experienceNumber"
      label="Number of  experiences"
      rules={[{ required: true }]}
      
    >
      <Input type="number"/>
    </Form.Item>

    <Form.Item>
      <Row gutter={[8, 16]}>
        <Col span={6}>
          <Button
            type="primary"
         
            disabled={currentSteps == 0}
            onClick={() => {
              setCurrentSteps(currentSteps - 1);
            }}
          >
            Back{" "}
          </Button>
        </Col>
        <Col span={6}>
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Col>
        <Col span={6}>
          <Button
            disabled={data.length==0}
            type="primary"
            onClick={() => {
              setCurrentSteps(1);
            }}
          >
            Next
          </Button>
        </Col>
      </Row>
    </Form.Item>
  </Form>
);

const AcademicForm = ({ setCurrentSteps, currentSteps ,update,data}: ownFormProps) => (
  <Form {...layout} onFinish={values=> update(values) }>
    <Form.Item
      name="schoolName"
      label="School Name"
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
    <Form.Item name="address" label="Address" rules={[{ required: true }]}>
      <Input />
    </Form.Item>

    <Form.Item
      name="description "
      label="Description"
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>

    <Form.Item>
      <Row gutter={[8, 16]}>
        <Col span={6}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={currentSteps == 0}
            onClick={() => {
              setCurrentSteps(currentSteps - 1);
            }}
          >
            Back{" "}
          </Button>
        </Col>
        <Col span={6}>
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Col>
        <Col span={6}>
          <Button
             disabled={data.length==0}

            type="primary"
            htmlType="submit"
            onClick={() => {
              setCurrentSteps(2);
            }}
          >
            Next
          </Button>
        </Col>
      </Row>
    </Form.Item>
  </Form>
);
const TechnicalSkills = ({ setCurrentSteps, currentSteps,data,update }: ownFormProps) => (
  <Form {...layout} onFinish={values=> update(values) }>
    <Form.Item name="slill" label="Skill" rules={[{ required: true }]}>
      <Input />
    </Form.Item>
    <Form.Item
      name="certification"
      label="Certification"
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
    <Form.Item>
      <Row gutter={[8, 16]}>
        <Col span={6}>
          <Button
            type="primary"
         
            disabled={currentSteps == 0}
            onClick={() => {
              setCurrentSteps(currentSteps - 1);
            }}
          >
            Back{" "}
          </Button>
        </Col>
        <Col span={6}>
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Col>
        <Col span={6}>
          <Button
               disabled={data.length==0}
            type="primary"
            onClick={() => {
              setCurrentSteps(3);
            }}
          >
            Next
          </Button>
        </Col>
      </Row>
    </Form.Item>
  </Form>
);
const SoftSkills = ({ setCurrentSteps, currentSteps,data,update }: ownFormProps) => (
  <Form {...layout} onFinish={values=> update(values) }>
    <Form.Item name="skillName" label="Skill" rules={[{ required: true }]}>
      <Input />
    </Form.Item>
    <Form.Item
      name="experience"
      label="Experiences"
      rules={[{ required: true }]}
    >
      <Input  type="number"/>
    </Form.Item>

    <Form.Item>
      <Row gutter={[8, 16]}>
        <Col span={6}>
          <Button
            type="primary"
      
            disabled={currentSteps == 0}
            onClick={() => {
              setCurrentSteps(currentSteps - 1);
            }}
          >
            Back{" "}
          </Button>
        </Col>
        <Col span={6}>
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Col>
        <Col span={6}>
          <Button
            type="primary"
            disabled={data.length==0}
            onClick={() => {
              setCurrentSteps(4);
            }}
          >
            Next
          </Button>
        </Col>
      </Row>
    </Form.Item>
  </Form>
);






const PersonalInfo = ({ setCurrentSteps, currentSteps,data,update }: ownFormProps) => (
  <Form {...layout} onFinish={values=> update(values) }>
    <Form.Item name="firstName" label="firstName" rules={[{ required: true }]}>
      <Input />
    </Form.Item>
    <Form.Item
      name="lastName"
      label="lastName"
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
    <Form.Item
      name="age"
      label="Age"
      rules={[{ required: true }]}
    >
      <Input type="number"/>
    </Form.Item>
    <Form.Item
      name="address"
      label="Address"
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
    <Form.Item
      name="email"
      label="Email"
      rules={[{ required: true }]}
    >
      <Input type="email"/>
    </Form.Item>
    <Form.Item
      name="jobName"
      label="Actuel job (optional)"
      rules={[{ required: false }]}
    >
      <Input />
    </Form.Item>
    
    <Form.Item
      name="jobLokkingFor"
      label="Job looked for"
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
    <Form.Item
      name="status"
      label="Status(student or employee)"
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
    <Form.Item>
      <Row gutter={[8, 16]}>
        <Col span={6}>
          <Button
            type="primary"
      
            disabled={currentSteps == 0}
            onClick={() => {
              setCurrentSteps(currentSteps - 1);
            }}
          >
            Back{" "}
          </Button>
        </Col>
        <Col span={6}>
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Col>
        <Col span={6}>
          <Button
            type="primary"
            disabled={data.length==0}
         
          >
          Create
          </Button>
        </Col>
      </Row>
    </Form.Item>
  </Form>
);
const LetterCreation = ({
  updateExperienceAction,
  updatePersonalInfoAction,
  updateSoftSkillsAction,
  updateAcademicInfoAction,
  updateTechnicalSkillsAction,
  experiences,
  personalInfo,
  softSkills,
  technicalSkills,
  academicInfo
}: Props) => {
  const [currentSteps, setCurrentSteps] = React.useState(0);

  return (
    <Row gutter={[48, 16]} style={{ marginTop: "2%" }}>
      <Col span={10}>
        <Steps progressDot current={currentSteps} direction="vertical">
          <Step
            title="Experiences"
            description="You can put here your professional  experiences"
          />
          <Step
            title="Academic training"
            description="You can put your academic training here"
          />
          <Step
            title="techniqual skills"
            description="You can put your technical skills (java ...)"
          />
          <Step
            title="Soft skills"
            description="You can put your soft skills (communications ...) "
          />
          <Step
            title="Personal informations"
            description="You can put here your personal informations (name , age ...)"
          />
        </Steps>
      </Col>
      <Col span={14}>
        {currentSteps == 0 ? (
          <ExperienceForm
          data={experiences}
            update={updateExperienceAction}
            setCurrentSteps={setCurrentSteps}
            currentSteps={currentSteps}
          />
        ) : currentSteps == 1 ? (
          <AcademicForm
          data={academicInfo}
            update={updateAcademicInfoAction}
            setCurrentSteps={setCurrentSteps}
            currentSteps={currentSteps}
          />
        ) : currentSteps == 2 ? (
          <TechnicalSkills
          data={technicalSkills}
            update={updateTechnicalSkillsAction}
            setCurrentSteps={setCurrentSteps}
            currentSteps={currentSteps}
          />
        ) 
        
        : currentSteps == 3 ? (
          <SoftSkills
          data={softSkills}
            update={updateSoftSkillsAction}
            setCurrentSteps={setCurrentSteps}
            currentSteps={currentSteps}
          />
        )
        
        : (
          <PersonalInfo
            data={personalInfo}
            update={updatePersonalInfoAction}
            setCurrentSteps={setCurrentSteps}
            currentSteps={currentSteps}
          />
        )}
      </Col>
    </Row>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(LetterCreation);
